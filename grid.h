#pragma once

#include "gridview.h"
#include "symmetry.h"
#include <vector>
#include <iosfwd>

struct Grid
{
    uint8_t w, h;
    std::vector<bool> squares;
    
    GridView view() const {return GridView(w, h);}
    uint16_t index(uint8_t x, uint8_t y) const {return view().index(x, y);}
    uint16_t index(Coord p)              const {return index(p.x, p.y);}

    void invert();
    void trim();

    bool isrowempty(uint8_t y) const;
    bool iscolempty(uint8_t x) const;

    std::vector<std::pair<Coord, uint8_t>> adjacents() const;
    SymmetryGroup symmetry_subgroup(SymmetryGroup g) const;
    uint16_t count() const;
    
    void read (std::istream&);
    void write(std::ostream&) const;
};

bool compare_gridviews(const Grid &g1, GridView v1, const Grid& g2, GridView v2);
