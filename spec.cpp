#include "spec.h"
#include <istream>
#include <ostream>
#include <string>
#include <iomanip>

void read_colour(uint32_t &col, std::istream &in)
{
    int r, g, b; in >> r >> g >> b;
    r &= 0xff; g &= 0xff; b &= 0xff;
    col = r | (g << 8) | (b << 16);
}

void write_colour(uint32_t col, std::ostream &out)
{
    uint8_t r =  col        & 0xff;
    uint8_t g = (col >>  8) & 0xff;
    uint8_t b = (col >> 16) & 0xff;
    out << (int)r << ' ' << (int)g << ' ' << (int)b;
}

void read_symmetrygroup(SymmetryGroup &g, std::istream &in)
{
    std::string str;

    in >> std::ws;
    std::getline(in, str);

    for (uint8_t s = (uint8_t)SymmetryGroup::trivial; s <= (uint8_t)SymmetryGroup::full; s++)
        if (str == symmetrygroup_name((SymmetryGroup)s))
        {
            g = (SymmetryGroup)s;
            return;
        }

    in.setstate(std::ios::failbit);
}

void Spec::read(std::istream &in)
{
    grid.read(in);

    read_symmetrygroup(symmetrygroup, in);
    
    int num_pieces; in >> num_pieces;
    if (num_pieces < 0 || num_pieces > 1000) {in.setstate(std::ios::failbit); return;}
    
    pieces.resize(num_pieces);
    for (auto &p : pieces)
    {
        p.grid.read(in);
        
        int num_colours; in >> num_colours;
        if (num_colours < 0 || num_colours > 100) {in.setstate(std::ios::failbit); return;}
        
        p.colours.resize(num_colours);
        for (auto &c : p.colours)
            read_colour(c, in);
    }
}

void Spec::write(std::ostream &out) const
{
    grid.write(out);
    out << "\n\n" << symmetrygroup_name(symmetrygroup) << '\n' << pieces.size();
    
    for (auto &p : pieces)
    {
        out << "\n\n";
        p.grid.write(out);
        
        out << '\n' << p.colours.size();
        for (auto c : p.colours)
        {
            out << '\n';
            write_colour(c, out);
        }
    }
    
    out << '\n';
}
