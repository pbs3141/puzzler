#pragma once

#include "range.h"
#include <cstdint>

// Symmetries

enum class Symmetry : uint8_t
{
    id,
    rot90,
    rot180,
    rot270,
    refx,
    refmdiag,
    refy,
    refpdiag
};

// Symmetry groups

enum class SymmetryGroup : uint8_t
{
    trivial,
    rot180,
    rot90,
    refx,
    refy,
    refpdiag,
    refmdiag,
    refxy,
    refdiag,
    full
};

// Composition of symmetries

Symmetry symmetry_composition(Symmetry s1, Symmetry s2);
Symmetry symmetry_inverse    (Symmetry s);

// Names

const char* symmetry_name     (Symmetry      s);
const char* symmetrygroup_name(SymmetryGroup g);

// Symmetry group elements

range<const Symmetry*> symmetrygroup_elements           (SymmetryGroup g);
range<const Symmetry*> symmetrygroup_elements_nontrivial(SymmetryGroup g);

// Symmetry group bitsets

uint8_t symmetry_bit(Symmetry s);

SymmetryGroup symmetrygroup_from_bitset(uint8_t bitset);
uint8_t       symmetrygroup_to_bitset(SymmetryGroup g);

SymmetryGroup symmetrygroup_conjugate(SymmetryGroup g, Symmetry h);
SymmetryGroup symmetrygroup_intersect(SymmetryGroup g1, SymmetryGroup g2);
