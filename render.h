#pragma once

#include "processedspec.h"
#include "solution.h"
#include "polygons.h"

void render_solutions(const char *filename, const SolutionSet &solutionset, const ProcessedSpec &ps, const SpecPolygons &polygons);
