#include "gridview.h"

GridView GridView::act(Symmetry s) const
{
    switch (s)
    {
        case Symmetry::id:       return *this;
        case Symmetry::rot90:    return GridView(h, w, off                + dy * (h - 1), -dy,  dx);
        case Symmetry::rot180:   return GridView(w, h, off + dx * (w - 1) + dy * (h - 1), -dx, -dy);
        case Symmetry::rot270:   return GridView(h, w, off + dx * (w - 1)               ,  dy, -dx);
        case Symmetry::refx:     return GridView(w, h, off + dx * (w - 1)               , -dx,  dy);
        case Symmetry::refy:     return GridView(w, h, off                + dy * (h - 1),  dx, -dy);
        case Symmetry::refpdiag: return GridView(h, w, off                              ,  dy,  dx);
        case Symmetry::refmdiag: return GridView(h, w, off + dx * (w - 1) + dy * (h - 1), -dy, -dx);
    }
}
