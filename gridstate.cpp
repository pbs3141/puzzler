#include "gridstate.h"
#include <cassert>

/*
 * Layout:
 *  4     × Cell : Linked list heads
 *  w * h × Cell : Grid cells
 */

namespace {

struct Cell
{
    uint16_t prev, next;
    uint8_t adj;
    bool filled;
};

}

std::size_t GridState::alignment()
{
    return alignof(Cell);
}

std::size_t GridState::size(const Grid &grid)
{
    return (4 + grid.w * grid.h) * sizeof(Cell);
}

void GridState::init(const Grid &grid, uintptr_t m)
{
    auto data = reinterpret_cast<Cell*>(m);
    auto cells = data + 4;

    // Initialise memory for the heads of each of the 4 circular linked lists corresponding to adjacences 1...4
    for (uint8_t a = 0; a < 4; a++)
        new (data + a) Cell();

    // Initialise memory for grid
    std::size_t i = 0;

    for (uint8_t y = 0; y < grid.h; y++)
        for (uint8_t x = 0; x < grid.w; x++)
        {
            new (cells + i) Cell();
            i++;
        }
}

void GridState::reset_state(const Grid &grid, uintptr_t m)
{
    auto data = reinterpret_cast<Cell*>(m);
    auto cells = data + 4;

    // Copy grid, and compute adjacencies
    std::size_t i = 0;

    for (uint8_t y = 0; y < grid.h; y++)
        for (uint8_t x = 0; x < grid.w; x++)
        {
            cells[i].filled = grid.squares[i];

            cells[i].adj = 0;
            if (x == 0          || grid.squares[i - 1     ]) cells[i].adj++;
            if (x == grid.w - 1 || grid.squares[i + 1     ]) cells[i].adj++;
            if (y == 0          || grid.squares[i - grid.w]) cells[i].adj++;
            if (y == grid.h - 1 || grid.squares[i + grid.w]) cells[i].adj++;

            i++;
        }

    // Build the linked lists
    uint16_t cur[4] = {0, 1, 2, 3};

    for (std::size_t i = 0; i < grid.w * grid.h; i++)
        if (!cells[i].filled && cells[i].adj > 0)
        {
            uint8_t a = cells[i].adj - 1;
            assert(a < 4);

            data[cur[a]].next = 4 + i;
            data[4 + i ].prev = cur[a];
            cur[a] = 4 + i;
        }

    for (uint8_t a = 0; a < 4; a++)
    {
        data[cur[a]].next = a;
        data[a].prev = cur[a];
    }
}

void GridState::copy_state(uint16_t num_cells, uintptr_t from, uintptr_t m)
{
    auto data = reinterpret_cast<Cell*>(m);
    auto cells = data + 4;

    auto fdata = reinterpret_cast<Cell*>(from);
    auto fcells = fdata + 4;

    for (uint8_t a = 0; a < 4; a++)
    {
        data[a].next = fdata[a].next;
        data[a].prev = fdata[a].prev;
    }

    for (std::size_t i = 0; i < num_cells; i++)
        cells[i] = fcells[i];
}

void GridState::fill(uint16_t i, uintptr_t m)
{
    auto data = reinterpret_cast<Cell*>(m);
    auto &c = data[4 + i];

    assert(!c.filled);

    c.filled = true;

    // Unlink if necessary
    if (c.adj > 0)
    {
        data[c.prev].next = c.next;
        data[c.next].prev = c.prev;
    }
}

void GridState::unfill(uint16_t i, uintptr_t m)
{
    auto data = reinterpret_cast<Cell*>(m);
    auto &c = data[4 + i];

    assert(c.filled);

    c.filled = false;

    // Reinsert at the start of the linked list, if necessary
    if (c.adj > 0)
    {
        uint8_t a = c.adj - 1;
        assert(a < 4);

        c.prev = a;
        c.next = data[a].next;
        data[c.prev].next = 4 + i;
        data[c.next].prev = 4 + i;
    }
}

void GridState::adj(uint16_t i, uint8_t da, uintptr_t m)
{
    auto data = reinterpret_cast<Cell*>(m);
    auto &c = data[4 + i];

    if (c.filled) return;

    // Unlink if necessary
    if (c.adj > 0)
    {
        data[c.prev].next = c.next;
        data[c.next].prev = c.prev;
    }

    c.adj += da;
    assert(1 <= c.adj && c.adj <= 4);

    // Insert at start of the list corresponding to the new adjacency
    uint8_t a = c.adj - 1;

    c.prev = a;
    c.next = data[a].next;
    data[c.prev].next = 4 + i;
    data[c.next].prev = 4 + i;
}

void GridState::unadj(uint16_t i, uint8_t da, uintptr_t m)
{
    auto data = reinterpret_cast<Cell*>(m);
    auto &c = data[4 + i];

    if (c.filled) return;

    assert(c.adj >= da);

    // Unlink
    data[c.prev].next = c.next;
    data[c.next].prev = c.prev;

    c.adj -= da;

    // Insert at start of the list corresponding to the new adjacency (if necessary)
    if (c.adj > 0)
    {
        uint8_t a = c.adj - 1;
        assert(a < 4);

        c.prev = a;
        c.next = data[a].next;
        data[c.prev].next = 4 + i;
        data[c.next].prev = 4 + i;
    }
}

bool GridState::filled(uint16_t i, uintptr_t m)
{
    auto data = reinterpret_cast<const Cell*>(m);
    auto &c = data[4 + i];

    return c.filled;
}

uint8_t GridState::adj(uint16_t i, uintptr_t m)
{
    auto data = reinterpret_cast<const Cell*>(m);
    auto &c = data[4 + i];

    return c.adj;
}

int16_t GridState::most_surrounded(uintptr_t m)
{
    auto data = reinterpret_cast<const Cell*>(m);

    uint16_t a = 4, n;
    do
    {
        if (a == 0) return -1;
        a--;
        n = data[a].next;
    }
    while (n == a);
    
    assert(n >= 4);
    assert(!data[n].filled);
    return n - 4;
}
