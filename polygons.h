#pragma once

#include "processedspec.h"
#include <utility>

/*
 * For generating and storing sets of polygons used to draw puzzle pieces.
 */

struct PiecePolygons
{
    PiecePolygons() = default;
    PiecePolygons(const Grid &grid, double edge);
    
    using Coord = std::pair<double, double>;
    using Polygon = std::vector<Coord>;
    
    std::vector<Polygon> polygons;
};

struct SpecPolygons
{
    SpecPolygons() = default;
    SpecPolygons(const ProcessedSpec &ps, double edge);
    
    double edge;
    PiecePolygons grid;
    std::vector<PiecePolygons> pieces;
};
