#pragma once

template <typename T>
constexpr T _gcd(T a, T b)
{
    while (b != 0)
    {
        auto tmp = a % b;
        a = b;
        b = tmp;
    }
    return a;
}

template <typename T>
constexpr T gcd(T t)
{
    return t;
}

template <typename S, typename T, typename... U>
constexpr S gcd(S s, T t, U... u)
{
    return gcd(_gcd<S>(s, t), u...);
}

template <typename T>
constexpr T _lcm(T a, T b)
{
    return a * (b / _gcd(a, b));
}

template <typename T>
constexpr T lcm(T t)
{
    return t;
}

template <typename S, typename T, typename... U>
constexpr S lcm(S s, T t, U... u)
{
    return lcm(_lcm<S>(s, t), u...);
}

template <typename T, typename S>
constexpr void pad_to(T &t, S s)
{
    t += s - 1 - (t + s - 1) % s;
}
