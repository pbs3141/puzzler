#include "processedspec.h"
#include "subsetsums.h"
#include <unordered_map>
#include <functional>

ProcessedSpec::ProcessedSpec(const Spec &spec)
{
    // Copy data
    grid = spec.grid;
    symmetrygroup = spec.symmetrygroup;

    pieces.reserve(spec.pieces.size());
    for (auto &p : spec.pieces)
    {
        pieces.emplace_back();
        pieces.back().grid = p.grid;
        pieces.back().colours = p.colours;
    }

    // Trim background, count empty squares
    grid.invert();
    grid.trim();
    area = grid.count();
    grid.invert();

    // Trim pieces, and chuck out zero-sized pieces, or those with zero copies
    for (auto it = pieces.begin(); it != pieces.end();)
    {
        it->grid.trim();
        if (it->grid.squares.empty() || it->colours.empty()) {it = pieces.erase(it); continue;}
        ++it;
    }

    // Compute transformations of each piece and eliminate duplicate pieces
    for (auto it = pieces.begin(); it != pieces.end();)
    {
        // If symmetrically related to an earlier piece, merge this piece into it
        for (auto it2 = pieces.begin(); it2 != it; ++it2)
            for (auto s2 : it2->transformations)
                if (compare_gridviews(it->grid, it->grid.view(), it2->grid, it2->grid.view().act(s2)))
                {
                    it2->colours.insert(it2->colours.end(), it->colours.begin(), it->colours.end());
                    it = pieces.erase(it);
                    goto removed_piece;
                }

        // Record all distinct transformations
        for (auto s : symmetrygroup_elements(symmetrygroup))
        {
            for (auto s2 : it->transformations)
                if (compare_gridviews(it->grid, it->grid.view().act(s), it->grid, it->grid.view().act(s2)))
                    goto skip;

            it->transformations.emplace_back(s);
            skip:;
        }

        ++it;
        removed_piece:;
    }

    // Compute symmetry subgroup of background grid
    grid_subgroup = grid.symmetry_subgroup(symmetrygroup);

    // Compute each piece's area/adjacent squares/subgroup, and run subsetsums to find the admissible subsets
    std::vector<int> n, a;

    n.reserve(pieces.size());
    a.reserve(pieces.size());

    for (auto &p : pieces)
    {
        p.area = p.grid.count();
        p.adj = p.grid.adjacents();
        p.subgroup = p.grid.symmetry_subgroup(symmetrygroup);

        n.emplace_back(p.colours.size());
        a.emplace_back(p.area);
    }

    auto admissible_counts = subsetsums(n, a, area);

    // Fill out 'fits' for each piece
    for (auto &p : pieces)
        for (uint8_t i = 0; i < p.transformations.size(); i++)
        {
            auto sym = p.transformations[i];
            auto view = p.grid.view().act(sym);
            for (uint8_t y = 0; y < view.h; y++)
                for (uint8_t x = 0; x < view.w; x++)
                    if (p.grid.squares[view.index(x, y)])
                    {
                        bool xm = x > 0          ? p.grid.squares[view.index(x - 1, y    )] : false;
                        bool xp = x < view.w - 1 ? p.grid.squares[view.index(x + 1, y    )] : false;
                        bool ym = y > 0          ? p.grid.squares[view.index(x    , y - 1)] : false;
                        bool yp = y < view.h - 1 ? p.grid.squares[view.index(x    , y + 1)] : false;

                        for (int xmi = 0; xmi <= !xm; xmi++)
                            for (int xpi = 0; xpi <= !xp; xpi++)
                                for (int ymi = 0; ymi <= !ym; ymi++)
                                    for (int ypi = 0; ypi <= !yp; ypi++)
                                        if (xpi + xmi + ypi + ymi >= 2)
                                        {
                                            uint8_t surr = ypi | (xpi << 1) | (ymi << 2) | (xmi << 3);
                                            p.fits.push_back({surr, i, x, y});
                                        }
                    }
        }

    // Symmetry reduction - fills out 'subsets'

    // For each admissible set of piece counts...
    for (auto &counts : admissible_counts)
    {
        // ...start off with none of the pieces being marked as 'reduced', and 'transformations' to be filled in...
        std::vector<bool> reduced(pieces.size());
        std::vector<std::vector<Symmetry>> transformations(pieces.size());

        // ...and run the following recursively:
        std::function<void(SymmetryGroup)> search = [&, this] (SymmetryGroup residual)
        {
            int best_i = -1;
            std::unordered_map<SymmetryGroup, std::vector<Symmetry>> best_stabilisers;
            std::size_t best_maxstabilisersize;

            // For each piece with exactly one copy that has not yet been reduced...
            for (std::size_t i = 0; i < pieces.size(); i++)
                if (counts[i] == 1 && !reduced[i])
                {
                    // ...calculate all distinct orientations modulo the action of 'residual', and store these in 'orbitreps'.
                    std::vector<Symmetry> orbitreps;
                    uint8_t used = 0;
                    for (auto t : pieces[i].transformations)
                        if (!(used & symmetry_bit(t)))
                        {
                            orbitreps.emplace_back(t);
                            for (auto h : symmetrygroup_elements(pieces[i].subgroup))
                            {
                                auto t2 = symmetry_composition(t, h);
                                for (auto k : symmetrygroup_elements(residual))
                                {
                                    auto t3 = symmetry_composition(k, t2);
                                    used |= symmetry_bit(t3);
                                }
                            }
                        }

                    // For each orbit rep, find the stabiliser (the subgroup of 'residual' which still preserves it),
                    // and record for each distinct stabiliser the orbit reps that give rise to them.
                    std::unordered_map<SymmetryGroup, std::vector<Symmetry>> stabilisers;
                    for (auto o : orbitreps)
                    {
                        auto stab = symmetrygroup_intersect(residual, symmetrygroup_conjugate(pieces[i].subgroup, o));
                        stabilisers[stab].emplace_back(o);
                    }

                    // Calculate maximum stabiliser size (which we want to minimise), and check whether any stabiliser differs from 'residual' (i.e. whether any symmetry-reduction occurs)
                    std::size_t maxstabilisersize = 0;
                    bool trivial = true;
                    for (auto &stab : stabilisers)
                    {
                        if (stab.first != residual) trivial = false;
                        maxstabilisersize = std::max<std::size_t>(maxstabilisersize, symmetrygroup_elements(stab.first).size());
                    }

                    // This piece doesn't allow us to reduce 'residual' in any way - don't use this for symmetry-reduction
                    if (trivial) continue;

                    // Minimise number of different stabilisers, then maximum stabliser size (greedy algorithm tries to minimise the number of subsets generated)
                    if (   best_i == -1
                        || stabilisers.size() < best_stabilisers.size()
                        || (stabilisers.size() == best_stabilisers.size() && maxstabilisersize < best_maxstabilisersize)
                           )
                    {
                        best_i = i;
                        best_stabilisers = std::move(stabilisers);
                        best_maxstabilisersize = maxstabilisersize;
                    }
                }

            // If no pieces yielded symmetry-reduction, give up and write out an element of 'subsets'
            if (best_i == -1)
            {
                subsets.emplace_back();
                auto &subset = subsets.back();

                subset.counts.resize(pieces.size());
                subset.transformations.resize(pieces.size());
                for (std::size_t i = 0; i < pieces.size(); i++)
                {
                    subset.counts[i] = counts[i];
                    subset.transformations[i] = reduced[i] ? transformations[i] : pieces[i].transformations;
                }

                subset.residual = residual;

                return;
            }

            // Symmetry-reduce on the best piece, and recurse to reduce further
            reduced[best_i] = true;
            for (auto &stab : best_stabilisers)
            {
                transformations[best_i] = stab.second;
                search(stab.first);
            }
            reduced[best_i] = false;
        };

        search(grid_subgroup);
    }

    // Compute 'numpieces' and 'max_numpieces'
    max_numpieces = 0;

    for (auto &subset : subsets)
    {
        subset.numpieces = 0;
        for (auto c : subset.counts) subset.numpieces += c;
        max_numpieces = std::max(max_numpieces, subset.numpieces);
    }
}
