#pragma once

template<typename T>
struct range
{
    T b, e;
    auto begin() const {return b;}
    auto end  () const {return e;}
    auto size () const {return e - b;}
};

template<typename T>
range<T> make_range(T b, T e) {return {b, e};}

template<typename T, typename I>
range<T> make_sized_range(T b, I n) {return {b, b + n};}
