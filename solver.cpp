#include "solver.h"
#include "gridstate.h"
#include "piecestate.h"
#include "pieceinfo.h"
#include "gcd.h"
#include <memory>
#include <thread>
#include <mutex>
#include <atomic>
#include <cassert>

namespace {

// ------------------------------------------------------------------------- //

struct StatePtr
{
    uintptr_t gridstate;
    uintptr_t piecestate;
};

struct Frame
{
    // Indexes a range of ThreadData::stack
    uint16_t begin, end;
    uint16_t size() const {return end - begin;}

    // The contribution to the overall progress
    double dprogress;
};

struct Transition
{
    uint8_t piece;
    uint8_t tr;
    int8_t x, y;
};

struct ThreadData
{
    bool active = false;
    std::mutex mutex;

    double dprogress;
    int dsolutions;
    std::chrono::steady_clock::time_point update_time;

    int depth;
    StatePtr state;

    int head_depth;
    StatePtr head_state;
    Frame *frames;
    std::vector<Transition> stack;

    SolutionRecorder recorder;
    int block;

    Frame&      frame() {return frames[     depth];}
    Frame& head_frame() {return frames[head_depth];}
};

struct ResultBlock
{
    int thread, begin, end;
    int next;
};

/*
 * Layout for SearchState:
 *
 *  PieceInfo                     (SearchState::pieceinfo)
 *  ThreadData& × numthreads      (SearchState::perthread)
 *
 *  repeat (numthreads)
 *  {
 *      |                         (cacheline separator)
 *      ThreadData                (SearchState::perthread[i])
 *      GridState                 (ThreadData::state)
 *      PieceState                 "   "
 *      GridState                 (ThreadData::head_state)
 *      PieceState                 "        "
 *      Frame      × maxdepth     (ThreadData::frames)
 *  }
 */

class SearchState
{
    std::unique_ptr<char[]> buf;
    int numthreads;

public:
    SearchState(const ProcessedSpec &ps, int numthreads);
    ~SearchState();

    uintptr_t pieceinfo;
    ThreadData** perthread;
    std::vector<ResultBlock> resultblocks;
    int lastresultblock = -1;
};

SearchState::SearchState(const ProcessedSpec &ps, int numthreads) : numthreads(numthreads)
{
    /*
     * Here are the reasons for having to do such low-level memory management:
     *
     *  1) Improving cache-friendliness: the memory accessed from within the tightest loops should be allocated so
     *     as to reside in a single continguous block...
     *
     *  2) Avoiding false-sharing: ...but because the code is multithreaded, it's very important to ensure each
     *     thread's writeable data is on a separate cache line from the rest of the data.
     *
     *  3) Many of the structures are dynamically-sized, and the dynamically-sized parts would have to be stored
     *     in dynamically-allocated memory, which causes us to lose control of where this memory is allocated, hence
     *     breaking 1) and 2).
     *
     *  4) Using custom allocators to solve 3) is a bad idea. It doesn't allow us to pre-allocate a block of
     *     memory that is guaranteed to be big enough, so might get fragmented, breaking 1). It also doesn't play
     *     well with 2), since it would have to have a way for inserting cacheline separators.
     */

    constexpr std::size_t cacheline_size = 64;

    int maxdepth = ps.max_numpieces;

    // Compute the alignment and size needed for the memory block
    std::size_t alignment = lcm(PieceInfo::alignment(), alignof(ThreadData*), cacheline_size, alignof(ThreadData), GridState::alignment(), PieceState::alignment(), alignof(Frame));

    std::size_t size = 0;
                                        size += PieceInfo::size(ps);
    pad_to(size, alignof(ThreadData*)); size += sizeof(ThreadData*) * numthreads;
    for (int i = 0; i < numthreads; i++)
    {
        pad_to(size, cacheline_size);
        pad_to(size, alignof(ThreadData));     size += sizeof(ThreadData);
        pad_to(size, alignof(GridState));      size += GridState::size(ps.grid);
        pad_to(size, PieceState::alignment()); size += PieceState::size(ps);
        pad_to(size, GridState::alignment());  size += GridState::size(ps.grid);
        pad_to(size, PieceState::alignment()); size += PieceState::size(ps);
        pad_to(size, alignof(Frame));          size += sizeof(Frame) * maxdepth;
    }
    pad_to(size, cacheline_size);

    // Allocate the memory block on which the data structures will be constructed
    buf = std::unique_ptr<char[]>(new char[size + alignment]);
    uintptr_t m = reinterpret_cast<uintptr_t>(buf.get());
    pad_to(m, alignment);

    // Construct data structures and store pointers to them
    pieceinfo = m;
    PieceInfo::init(ps, m);
    m += PieceInfo::size(ps);

    pad_to(m, alignof(ThreadData*));
    perthread = reinterpret_cast<ThreadData**>(m);
    m += sizeof(ThreadData*) * numthreads;

    for (int i = 0; i < numthreads; i++)
    {
        pad_to(m, cacheline_size);

        pad_to(m, alignof(ThreadData));
        perthread[i] = reinterpret_cast<ThreadData*>(m);
        new (perthread[i]) ThreadData();
        m += sizeof(ThreadData);

        pad_to(m, GridState::alignment());
        perthread[i]->state.gridstate = m;
        GridState::init(ps.grid, m);
        m += GridState::size(ps.grid);

        pad_to(m, PieceState::alignment());
        perthread[i]->state.piecestate = m;
        PieceState::init(ps, m);
        m += PieceState::size(ps);

        pad_to(m, GridState::alignment());
        perthread[i]->head_state.gridstate = m;
        GridState::init(ps.grid, m);
        m += GridState::size(ps.grid);

        pad_to(m, PieceState::alignment());
        perthread[i]->head_state.piecestate = m;
        PieceState::init(ps, m);
        m += PieceState::size(ps);

        pad_to(m, alignof(Frame));
        perthread[i]->frames = reinterpret_cast<Frame*>(m);
        for (int j = 0; j < maxdepth; j++)
            new (&perthread[i]->frames[j]) Frame();
        m += sizeof(Frame) * maxdepth;
    }
}

SearchState::~SearchState()
{
    for (int i = 0; i < numthreads; i++)
    {
        perthread[i]->~ThreadData();
    }
}

// ------------------------------------------------------------------------- //

class Solver
{
    // The puzzle to be solved
    const ProcessedSpec &ps;

    // Interactivity
    int update_interval; // Responsiveness settings
    int max_response;    // ^
    double progress;     // Progress information
    int num_solutions;   // ^
    std::atomic_bool cancelled;      // Set when callback returns true
    std::function<bool(int, double)> callback; // For reporting progress and checking cancellation

    // Threading
    int numthreads;
    std::mutex mutex; // Protects 'progress' + 'num_solutions' + 'callback'

    // All the state and precomputed information
    SearchState state;

    // The maximum depth of the search, equal to the number of pieces in the subset under condsideration
    int maxdepth;

    // Slowsearch
    void reset_state(StatePtr stateptr) const;
    void copy_state(StatePtr from, StatePtr to) const;
    void inc_state(StatePtr stateptr, Transition transition) const;
    void dec_state(StatePtr stateptr, Transition transition) const;

    void expand(ThreadData &data, double dprogress);
    void process(ThreadData &data);
    void assist(int id);
    void search(double dprogress);

    void progress_update();

    // Fastsearch
    struct FastsearchData
    {
        uintptr_t pieceinfo, gridstate, piecestate;
        uint8_t width, height, numpieces;
        SolutionRecorder &recorder;
    };

    void start_fastsearch(ThreadData &data);
    static void fastsearch(const FastsearchData &f);

public:
    Solver(const ProcessedSpec &ps, std::function<bool(int, double)> callback, int numthreads, int update_interval, int max_response);

    SolutionSet get_solutionset() const;
};

}

Solver::Solver(const ProcessedSpec &ps, std::function<bool(int, double)> callback, int numthreads, int min_response, int max_response) : ps(ps), update_interval(min_response * numthreads), max_response(max_response), progress(0.0), num_solutions(0), cancelled(false), callback(callback), numthreads(numthreads), state(ps, numthreads)
{
    if (ps.subsets.empty()) {callback(0, 1.0); return;}

    auto current_time = std::chrono::steady_clock::now();
    for (int i = 0; i < numthreads; i++)
    {
        auto &data = *state.perthread[i];
        data.update_time = current_time + std::chrono::milliseconds(min_response) * (i + 1);
        data.dprogress = 0.0;
        data.dsolutions = 0;
    }

    double dprogress = 1.0 / ps.subsets.size();
    for (auto &subset : ps.subsets)
    {
        // Load the subset
        for (std::size_t i = 0; i < ps.pieces.size(); i++)
            PieceInfo::update_transformations(i, subset.transformations[i], ps, state.pieceinfo);

        for (int i = 0; i < numthreads; i++)
        {
            auto &data = *state.perthread[i];
            PieceState::update_counts(subset.counts, data.     state.piecestate);
            PieceState::update_counts(subset.counts, data.head_state.piecestate);
        }

        maxdepth = subset.numpieces;

        /*
        if (subset.residual != SymmetryGroup::trivial)
        {
            // Todo: Find a way to prevent duplicate solutions for puzzles with residual symmetry
            // ...
        }
        */

        // Start searching from a blank state
        search(dprogress);
    }

    for (int i = 0; i < numthreads; i++)
    {
        auto &data = *state.perthread[i];
        progress += data.dprogress;
        num_solutions += data.dsolutions;
    }
    progress_update();
}

void Solver::reset_state(StatePtr stateptr) const
{
    GridState ::reset_state(ps.grid,          stateptr.gridstate);
    PieceState::reset_state(ps.pieces.size(), stateptr.piecestate);
}

void Solver::copy_state(StatePtr from, StatePtr to) const
{
    GridState ::copy_state(ps.grid.w * ps.grid.h, from.gridstate,  to.gridstate);
    PieceState::copy_state(ps.pieces.size(),      from.piecestate, to.piecestate);
}

void Solver::inc_state(StatePtr stateptr, Transition transition) const
{
    auto &piece = PieceInfo::pieces(state.pieceinfo)[transition.piece];
    auto &transformedpiece = piece.trans[transition.tr];

    // Place into grid
    for (auto s : make_sized_range(transformedpiece.squares, piece.num_squares))
        GridState::fill(transition.x + s.x + ps.grid.w * (transition.y + s.y), stateptr.gridstate);

    for (auto a : make_sized_range(transformedpiece.adj, piece.num_adj))
    {
        int8_t x = transition.x + a.first.x;
        int8_t y = transition.y + a.first.y;
        if (x >= 0 && y >= 0 && x < ps.grid.w && y < ps.grid.h)
            GridState::adj(x + ps.grid.w * y, a.second, stateptr.gridstate);
    }

    // Mark the piece as used
    PieceState::place(transition.piece, transition.x, transition.y, transformedpiece.sym, stateptr.piecestate);
}

void Solver::dec_state(StatePtr stateptr, Transition transition) const
{
    auto &piece = PieceInfo::pieces(state.pieceinfo)[transition.piece];
    auto &transformedpiece = piece.trans[transition.tr];

    // Unmark the piece as used
    PieceState::unplace(transition.piece, stateptr.piecestate);

    // Remove from the grid
    for (auto a : make_sized_range(transformedpiece.adj, piece.num_adj))
    {
        int8_t x = transition.x + a.first.x;
        int8_t y = transition.y + a.first.y;
        if (x >= 0 && y >= 0 && x < ps.grid.w && y < ps.grid.h)
            GridState::unadj(x + ps.grid.w * y, a.second, stateptr.gridstate);
    }

    for (auto s : make_sized_range(transformedpiece.squares, piece.num_squares))
        GridState::unfill(transition.x + s.x + ps.grid.w * (transition.y + s.y), stateptr.gridstate);
}

void Solver::search(double dprogress)
{
    // Set up main thread with all of the work
    constexpr int main_index = 0;
    ThreadData &main_data = *state.perthread[main_index];

    main_data.mutex.lock();
    main_data.active = true;
    main_data.head_depth = main_data.depth = 0;  // change these to start from non-blank state
    reset_state(main_data.head_state);           // ^
    copy_state(main_data.head_state, main_data.state);

    // Append a new ResultBlock for the main thread to write into
    state.resultblocks.push_back({main_index, (int)main_data.recorder.num_solutions(), {}, -1});
    main_data.block = state.resultblocks.size() - 1;

    if (state.lastresultblock != -1) state.resultblocks[state.lastresultblock].next = main_data.block;
    state.lastresultblock = main_data.block;

    // Launch extra threads in assist mode
    std::vector<std::thread> threads;
    for (int i = 1; i < numthreads; i++)
        threads.emplace_back([i, this] {assist(i);});

    // Run main thread
    expand(main_data, dprogress);
    state.resultblocks[main_data.block].end = main_data.recorder.num_solutions();

    // Run main thread in assist mode
    main_data.active = false;
    main_data.mutex.unlock();
    assist(main_index);

    // Ensure extra threads have terminated
    for (auto &thread : threads)
        thread.join();
}

void Solver::expand(ThreadData &data, double dprogress)
{
    uintptr_t gridstate  = data.state.gridstate;
    uintptr_t piecestate = data.state.piecestate;
    uint8_t width  = ps.grid.w;
    uint8_t height = ps.grid.h;

    // Find the most-surrounded square
    auto ms_index = GridState::most_surrounded(gridstate);
    auto ms_coord = Coord(ms_index % width, ms_index / width);

    // Begin a new stack frame
    auto &frame = data.frame();
    auto origstacksize = frame.begin = data.stack.size();

    // Compute the surround coordinate of the most-surrounded square
    bool xm = ms_coord.x > 0          ? GridState::filled(ms_index - 1,     gridstate) : true;
    bool xp = ms_coord.x < width - 1  ? GridState::filled(ms_index + 1,     gridstate) : true;
    bool ym = ms_coord.y > 0          ? GridState::filled(ms_index - width, gridstate) : true;
    bool yp = ms_coord.y < height - 1 ? GridState::filled(ms_index + width, gridstate) : true;
    uint8_t surr = yp | (xp << 1) | (ym << 2) | (xm << 3);

    // For all unused pieces, list all ways of placing them in the most-surrounded square
    for (auto handle = PieceState::begin_handle(piecestate); !PieceState::is_end_handle(handle); handle = PieceState::next_handle(handle, piecestate))
    {
        uint8_t p = PieceState::index_handle(handle);
        auto &piece = PieceInfo::pieces(state.pieceinfo)[p];

        assert(surr > 0);
        for (auto fit : make_range(piece.fits + piece.lookup[surr - 1], piece.fits + piece.lookup[surr]))
        {
            // Try placing piece 'p' (transformed and translated as described by 'f') in the most-surrounded square
            auto &t = piece.trans[fit.tr];
            int8_t x = ms_coord.x - fit.x;
            int8_t y = ms_coord.y - fit.y;

            // Check within grid bounds
            if (x < 0 || y < 0 || x + t.w > width || y + t.h > height) continue;

            // Check all the squares are empty
            for (auto s2 : make_sized_range(t.squares, piece.num_squares))
                if (GridState::filled(x + s2.x + width * (y + s2.y), gridstate))
                    goto skip;

            data.stack.push_back({p, fit.tr, x, y});

            skip:;
        }
    }

    // End the current stack frame
    frame.end = data.stack.size();

    if (frame.size() > 0)
    {
        // Set the 'dprogress' of the children
        frame.dprogress = dprogress / frame.size();

        // Process the transitions on the current stack frame
        process(data);
    }
    else
    {
        // Use up all the 'dprogress' allocated to this frame
        data.dprogress += dprogress;
    }

    // Clear the transitions from the stack
    data.stack.resize(origstacksize);
}

void Solver::process(ThreadData &data)
{
    auto &frame = data.frame();

    auto start_time = std::chrono::steady_clock::now();
    int num_processed = 0;

    // For each transition at the current depth yet to be processed
    while (frame.begin < frame.end)
    {
        // Enable fastsearch automatically for the last few levels of the tree, or if average of past searches was fast enough
        bool fast_mode = false;

        constexpr int num_fast_layers = 4;
        fast_mode |= data.depth >= maxdepth - 1 - num_fast_layers;

        if (num_processed > 0)
        {
            int time_spent = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start_time).count();
            fast_mode |= time_spent / num_processed <= max_response;
        }

        // Apply transition to state
        inc_state(data.state, data.stack[frame.begin]);

        if (fast_mode)
        {
            // Release lock and process the current state in fast mode
            data.mutex.unlock();

            auto orig_solutions_size = data.recorder.num_solutions();

            start_fastsearch(data);

            data.dprogress += frame.dprogress;
            data.dsolutions += data.recorder.num_solutions() - orig_solutions_size;

            data.mutex.lock();
        }
        else
        {
            // Otherwise, continue searching in slow mode
            data.depth++;
            expand(data, frame.dprogress);
            data.depth--;
        }

        // Unapply transition from state
        dec_state(data.state, data.stack[frame.begin]);

        frame.begin++;
        num_processed++;

        // Update progress if enough time has passed since the last update
        auto current_time = std::chrono::steady_clock::now();
        if (current_time >= data.update_time)
        {
            do data.update_time += std::chrono::milliseconds(update_interval); while (current_time >= data.update_time);

            mutex.lock();
            progress += data.dprogress;
            num_solutions += data.dsolutions;
            progress_update();
            mutex.unlock();

            data.dprogress = 0.0;
            data.dsolutions = 0;
        }

        if (cancelled) return;
    }
}

void Solver::assist(int id)
{
    auto &data = *state.perthread[id];
    assert(!data.active);
    assert(data.stack.empty());

    // Acquire all the threads' locks in order
    for (int i = 0; i < numthreads; i++)
        state.perthread[i]->mutex.lock();

    while (true)
    {
        // Determine the active thread whose head is at the smallest depth, then with the most work at that depth
        int best_depth;
        int best_work;
        int best_i = -1;

        for (int i = 0; i < numthreads; i++)
        {{
            auto &data2 = *state.perthread[i];
            if (!data2.active) goto skip;

            // Lazily move the head down to the first depth at which there is work to steal, or skip if there is none
            while (true)
            {
                if (data2.head_depth > data2.depth) goto skip;
                if (data2.head_frame().size() > 1) break;
                assert(data2.head_frame().size() == 1);
                inc_state(data2.head_state, data2.stack[data2.head_frame().begin]);
                data2.head_depth++;
            }

            // Keep record of best thread
            int depth = data2.head_depth;
            int work = data2.head_frame().size();
            if (best_i == -1 || depth < best_depth || (depth == best_depth && work > best_work))
            {
                best_depth = depth;
                best_work = work;
                best_i = i;
            }
        }skip:;}

        // If no work to steal, exit and leave the remaining threads to finish it off
        if (best_i == -1) break;

        // Steal exactly half its remaining work (guaranteed to leave at least one item of work)
        int steal = best_work - (best_work / 2);
        auto &steal_data = *state.perthread[best_i];

        assert(!data.active);
        data.active = true;
        data.head_depth = data.depth = steal_data.head_depth;
        copy_state(steal_data.head_state, data.head_state);
        copy_state(steal_data.head_state, data.state);

        assert(data.stack.empty());
        data.stack.resize(steal);
        data.head_frame() = {0, (uint16_t)steal, steal_data.head_frame().dprogress};

        for (int i = 0; i < steal; i++)
            data.stack[i] = std::move(steal_data.stack[steal_data.head_frame().end - steal + i]);

        steal_data.head_frame().end -= steal;

        // Append new ResultBlock for this thread to write into
        state.resultblocks.push_back({id, (int)data.recorder.num_solutions(), {}, state.resultblocks[steal_data.block].next});
        data.block = state.resultblocks.size() - 1;

        state.resultblocks[steal_data.block].next = data.block;
        if (steal_data.block == state.lastresultblock) state.lastresultblock = data.block;

        // Unlock all other threads
        for (int i = 0; i < numthreads; i++)
            if (i != id)
                state.perthread[i]->mutex.unlock();

        // Call process
        process(data);

        // Clear what we placed on the stack, and reset thread back to inactive
        data.active = false;
        data.stack.clear();

        // Unlock the remaining mutex (crucial to avoid deadlock, even though (almost) immediately re-locked!)
        data.mutex.unlock();

        // Record the last search's solution count in its result block (can't do this while searching, because the list of result blocks is shared)
        state.resultblocks[data.block].end = data.recorder.num_solutions();

        // Acquire all the threads' locks in order
        for (int i = 0; i < numthreads; i++)
            state.perthread[i]->mutex.lock();
    }

    // Release all the locks
    for (int i = 0; i < numthreads; i++)
        state.perthread[i]->mutex.unlock();
}

void Solver::start_fastsearch(ThreadData &data)
{
    auto f = FastsearchData{state.pieceinfo, data.state.gridstate, data.state.piecestate, ps.grid.w, ps.grid.h, (uint8_t)ps.pieces.size(), data.recorder};
    fastsearch(f);
}

void Solver::fastsearch(const FastsearchData &f)
{
    auto ms_index = GridState::most_surrounded(f.gridstate);
    auto ms_coord = Coord(ms_index % f.width, ms_index / f.width);

    // Check if found solution
    if (ms_index == -1)
    {
        PieceState::readout_solution(f.numpieces, f.recorder, f.piecestate);
        return;
    }

    bool xm = ms_coord.x > 0            ? GridState::filled(ms_index - 1,       f.gridstate) : true;
    bool xp = ms_coord.x < f.width - 1  ? GridState::filled(ms_index + 1,       f.gridstate) : true;
    bool ym = ms_coord.y > 0            ? GridState::filled(ms_index - f.width, f.gridstate) : true;
    bool yp = ms_coord.y < f.height - 1 ? GridState::filled(ms_index + f.width, f.gridstate) : true;
    uint8_t surr = yp | (xp << 1) | (ym << 2) | (xm << 3);

    for (auto handle = PieceState::begin_handle(f.piecestate); !PieceState::is_end_handle(handle); handle = PieceState::next_handle(handle, f.piecestate))
    {
        auto p = PieceState::index_handle(handle);
        auto &piece = PieceInfo::pieces(f.pieceinfo)[p];

        assert(surr > 0);
        for (auto fit : make_range(piece.fits + piece.lookup[surr - 1], piece.fits + piece.lookup[surr]))
        {
            auto &t = piece.trans[fit.tr];
            int8_t x = ms_coord.x - fit.x;
            int8_t y = ms_coord.y - fit.y;

            // Check within bounds
            if (x < 0 || y < 0 || x + t.w > f.width || y + t.h > f.height) continue;

            // Check squares are empty
            for (auto s2 : make_sized_range(t.squares, piece.num_squares))
                if (GridState::filled(x + s2.x + f.width * (y + s2.y), f.gridstate))
                    goto skip;

            // Fill squares
            for (auto s : make_sized_range(t.squares, piece.num_squares))
                GridState::fill(x + s.x + f.width * (y + s.y), f.gridstate);

            // Mark adjacents
            for (auto a : make_sized_range(t.adj, piece.num_adj))
            {
                int8_t x2 = x + a.first.x;
                int8_t y2 = y + a.first.y;
                if (x2 >= 0 && y2 >= 0 && x2 < f.width && y2 < f.height)
                    GridState::adj(x2 + f.width * y2, a.second, f.gridstate);
            }

            // Use piece
            PieceState::place(p, x, y, t.sym, f.piecestate);

            // Recurse
            fastsearch(f);

            // Unuse piece
            PieceState::unplace(p, f.piecestate);

            // Unmark adjacents
            for (auto a : make_sized_range(t.adj, piece.num_adj))
            {
                int8_t x2 = x + a.first.x;
                int8_t y2 = y + a.first.y;
                if (x2 >= 0 && y2 >= 0 && x2 < f.width && y2 < f.height)
                    GridState::unadj(x2 + f.width * y2, a.second, f.gridstate);
            }

            // Unfill squares
            for (auto s : make_sized_range(t.squares, piece.num_squares))
                GridState::unfill(x + s.x + f.width * (y + s.y), f.gridstate);

            skip:;
        }
    }
}

void Solver::progress_update()
{
    if (callback && callback(num_solutions, progress)) cancelled = true;
}

SolutionSet Solver::get_solutionset() const
{
    SolutionSet result;

    int i = 0;
    while (true)
    {
        auto &block = state.resultblocks[i];
        auto &data = *state.perthread[block.thread];

        for (int j = block.begin; j < block.end; j++)
        {
            // Copy the jth solution in 'data.recorder' to 'result'
            Solution sol;

            for (auto &item : data.recorder.get_solution(j))
                sol.items.emplace_back(item);

            result.solutions.emplace_back(std::move(sol));
        }

        i = block.next;
        if (i == -1) break;
    }

    return result;
}

SolutionSet solve(const ProcessedSpec &ps, std::function<bool(int, double)> callback)
{
    return Solver(ps, callback, std::thread::hardware_concurrency(), 50, 1000).get_solutionset();
}
