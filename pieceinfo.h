#pragma once

#include "processedspec.h"
#include <cstdint>

/*
 * A structure for holding shared information about all the pieces.
 * Used in a read-only way by all the search threads.
 */

struct PieceInfo
{
    static std::size_t alignment();
    static std::size_t size(const ProcessedSpec &ps);
    static void        init(const ProcessedSpec &ps, uintptr_t m);

    struct TransformedPiece;
    struct Fit;

    struct Piece
    {
        uint16_t num_squares;    // Number of squares (in every transformed piece)
        uint16_t num_adj;        // Number of adjacent squares (to every transformed piece)
        TransformedPiece *trans; // Transformed versions of the piece
        Fit *fits;               // The ways of fitting a transformed version of this piece into a surrounded square | variable, depends on the subset being considered
        uint8_t lookup[16];      // For each kind of surrounded square, a contiguous range indexed into 'fits'       | ^
    };

    struct Fit
    {
        uint8_t tr;   // The transformed piece (indexes into Piece::trans)
        uint8_t x, y; // Translation
    };

    using Adj = std::pair<Coord, uint8_t>;
    struct TransformedPiece
    {
        uint8_t w, h;   // Dimensions
        uint8_t sym;    // Symmetry that has been applied
        Coord *squares; // Coordinates of squares contained inside
        Adj *adj;       // Coordinates of adjacent squares
    };

    static Piece* pieces(uintptr_t m);
    static void update_transformations(std::size_t i, const std::vector<Symmetry> &transformations, const ProcessedSpec &ps, uintptr_t m);
};
