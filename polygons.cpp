#include "polygons.h"
#include <array>

PiecePolygons::PiecePolygons(const Grid &grid, double edge)
{
    float inside = 1.0 - 2.0 * edge;
    
    enum class Corner : char {None, Convex, Concave};
    std::vector<std::array<Corner, 4>> corners(grid.squares.size(), {{Corner::None, Corner::None, Corner::None, Corner::None}});
    
    for (int8_t y = 0; y < grid.h; y++)
        for (int8_t x = 0; x < grid.w; x++)
        {
            uint16_t k = grid.index(x, y);
            
            if (grid.squares[k])
                for (int8_t dy = 0; dy <= 1; dy++)
                    for (int8_t dx = 0; dx <= 1; dx++)
                    {
                        int8_t dx2 = 2 * dx - 1;
                        int8_t dy2 = 2 * dy - 1;
                        bool xr = dx ? x < grid.w - 1 : x > 0;
                        bool yr = dy ? y < grid.h - 1 : y > 0;
                        bool gx  =  xr        ? grid.squares[k + dx2               ] : false;
                        bool gy  =        yr  ? grid.squares[k       + dy2 * grid.w] : false;
                        bool gxy = (xr && yr) ? grid.squares[k + dx2 + dy2 * grid.w] : false;
                        
                        corners[k][dx + 2 * dy] = !gx && !gy         ? Corner::Convex
                                                :  gx &&  gy && !gxy ? Corner::Concave
                                                :                      Corner::None;
                    }
        }

    auto next_corner = [&] (int8_t &x, int8_t &y, int8_t &dx, int8_t &dy, Corner &corner)
    {
        int8_t mx, my;
        if (corner == Corner::Convex)
        {
            mx = 1 - dx - dy;
            my = dx - dy;
        }
        else/* corner == Corner::Concave */
        {
            mx = dx - dy;
            my = dx + dy - 1;
        }

        do
        {
            dx += mx + 2; x += dx / 2 - 1; dx %= 2;
            dy += my + 2; y += dy / 2 - 1; dy %= 2;
            corner = corners[grid.index(x, y)][dx + 2 * dy];
        }
        while (corner == Corner::None);
    };
    
    auto trace_boundary = [&, this] (int8_t x, int8_t y, int8_t dx, int8_t dy)
    {
        int8_t x0 = x, y0 = y, dx0 = dx, dy0 = dy;

        polygons.emplace_back();

        auto corner = corners[grid.index(x, y)][dx + 2 * dy];
        do
        {
            next_corner(x, y, dx, dy, corner);
            corners[grid.index(x, y)][dx + 2 * dy] = Corner::None;
            polygons.back().emplace_back(x + edge + dx * inside, y + edge + dy * inside);
        }
        while (!(x == x0 && y == y0 && dx == dx0 && dy == dy0));
    };

    for (int8_t y = 0; y < grid.h; y++)
        for (int8_t x = 0; x < grid.w; x++)
            if (grid.squares[grid.index(x, y)])
                for (int8_t dy = 0; dy <= 1; dy++)
                    for (int8_t dx = 0; dx <= 1; dx++)
                        if (corners[grid.index(x, y)][dx + 2 * dy] != Corner::None)
                            trace_boundary(x, y, dx, dy);
}

SpecPolygons::SpecPolygons(const ProcessedSpec &ps, double edge) : edge(edge)
{
    auto tmp = ps.grid;
    tmp.invert();
    grid = PiecePolygons(tmp, -edge);

    pieces.reserve(ps.pieces.size());

    for (auto &piece : ps.pieces)
        pieces.emplace_back(piece.grid, edge);
}
