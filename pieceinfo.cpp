#include "pieceinfo.h"
#include "gcd.h"
#include <cassert>

/*
 * Layout:
 *  N     × Piece            : Pieces
 *  Σtᵢ   × TransformedPiece : Transformations of the pieces
 *  Σtᵢsᵢ × Coord            : Squares inside
 *  Σtᵢaᵢ × Adj              : Squares adjacent
 *  Σfᵢ   × Fit              : Fits
 *
 * (N = the number of pieces)
 * (tᵢ = the size of 'transformations' for the ith piece)
 * (sᵢ = the number of squares used in the ith piece - i.e. 'area')
 * (aᵢ = the number of adjacent squares for the ith piece - i.e. 'adj')
 * (fᵢ = the total number of fits for piece i)
 */

std::size_t PieceInfo::alignment()
{
    return lcm(alignof(Piece), alignof(TransformedPiece), alignof(Coord), alignof(Adj), alignof(Fit));
}

std::size_t PieceInfo::size(const ProcessedSpec &ps)
{
    std::size_t total_transformations = 0;
    std::size_t total_squares = 0;
    std::size_t total_adj = 0;
    std::size_t total_fits = 0;

    for (auto &sp : ps.pieces)
    {
        total_transformations += sp.transformations.size();
        total_squares         += sp.transformations.size() * sp.area;
        total_adj             += sp.transformations.size() * sp.adj.size();
        total_fits            += sp.fits.size();
    }

    std::size_t size = 0;
    size += sizeof(Piece) * ps.pieces.size();
    pad_to(size, alignof(TransformedPiece));
    size += sizeof(TransformedPiece) * total_transformations;
    pad_to(size, alignof(Coord));
    size += sizeof(Coord) * total_squares;
    pad_to(size, alignof(Adj));
    size += sizeof(Adj) * total_adj;
    pad_to(size, alignof(Fit));
    size += sizeof(Fit) * total_fits;

    return size;
}

void PieceInfo::init(const ProcessedSpec &ps, uintptr_t m)
{
    std::size_t total_transformations = 0;
    std::size_t total_squares = 0;
    std::size_t total_adj = 0;

    for (auto &sp : ps.pieces)
    {
        total_transformations += sp.transformations.size();
        total_squares         += sp.transformations.size() * sp.area;
        total_adj             += sp.transformations.size() * sp.adj.size();
    }

    // Get pointers to various things
    auto pieces = reinterpret_cast<Piece*>(m);

    m += sizeof(Piece) * ps.pieces.size();
    pad_to(m, alignof(TransformedPiece));

    auto trans = reinterpret_cast<TransformedPiece*>(m);

    m += sizeof(TransformedPiece) * total_transformations;
    pad_to(m, alignof(Coord));

    auto squares = reinterpret_cast<Coord*>(m);

    m += sizeof(Coord) * total_squares;
    pad_to(m, alignof(Adj));

    auto adj = reinterpret_cast<Adj*>(m);

    m += sizeof(Adj) * total_adj;
    pad_to(m, alignof(Fit));

    auto fits = reinterpret_cast<Fit*>(m);

    // Initialise memory
    auto p = pieces;
    auto t = trans;
    auto s = squares;
    auto a = adj;
    auto f = fits;

    for (auto &sp : ps.pieces)
    {
        new (p) Piece();
        p->num_squares = sp.area;
        p->num_adj = sp.adj.size();

        p->trans = t;
        for (auto sym : sp.transformations)
        {
            new (t) TransformedPiece();

            auto view = sp.grid.view().act(sym);
            t->w = view.w;
            t->h = view.h;
            t->sym = (uint8_t)sym;

            t->squares = s;
            for (uint8_t x = 0; x < view.w; x++)
                for (uint8_t y = 0; y < view.h; y++)
                    if (sp.grid.squares[view.index(x, y)])
                        new (s++) Coord(x, y);

            t->adj = a;
            for (auto c : sp.adj)
                new (a++) Adj(c.first.act(sym, sp.grid.w, sp.grid.h), c.second);

            t++;
        }

        p->fits = f;
        for (std::size_t i = 0; i < sp.fits.size(); i++)
            new (f++) Fit();

        p++;
    }
}

PieceInfo::Piece* PieceInfo::pieces(uintptr_t m)
{
    return reinterpret_cast<Piece*>(m);
}

void PieceInfo::update_transformations(std::size_t i, const std::vector<Symmetry> &transformations, const ProcessedSpec &ps, uintptr_t m)
{
    uint8_t bitset = 0;
    for (auto sym : transformations)
        bitset |= symmetry_bit(sym);

    auto &sp = ps.pieces[i];
    auto &piece = pieces(m)[i];

    // Count number of Fits with admissible transformations of each square type
    uint8_t counts[16];
    for (int i = 0; i < 16; i++)
        counts[i] = 0;

    for (auto fit : sp.fits)
        if (symmetry_bit(sp.transformations[fit.tr]) & bitset)
            counts[fit.surr]++;

    // Write cumulative counts to Piece::lookup
    uint8_t cumulative = 0;
    for (int i = 0; i < 16; i++)
        piece.lookup[i] = cumulative += counts[i];

    // Write out lists of Fits, and advance Piece::lookup to point to ends of ranges
    uint8_t cur[16];
    for (int i = 0; i < 16; i++)
        cur[i] = piece.lookup[i];

    for (auto fit : sp.fits)
        if (symmetry_bit(sp.transformations[fit.tr]) & bitset)
            piece.fits[--cur[fit.surr]] = {fit.tr, fit.x, fit.y};
}
