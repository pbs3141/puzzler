#include "piecestate.h"
#include "gcd.h"
#include <cassert>

/*
 * Layout:
 *  1   × Piece          : Linked list head
 *  N   × Piece          : Pieces
 *  Σnᵢ × Transformation : Transformations applied to used pieces
 *
 * (N = the number of pieces)
 * (nᵢ = the maximum number of copies of the ith piece that will ever be used, irrespective of the effective piece counts)
 */

namespace {

struct Transformation;

struct Piece
{
    uint16_t        used;       // How many copies of this piece have been used
    uint16_t        count;      // How many copies we are allowed to use (the "effective piece count")
    uint16_t        prev, next; // Linked list of pieces with copies left to use (i.e. used < count)
    Transformation *trans;      // Pointer to the array of transformations to write to
};

struct Transformation
{
    uint8_t x, y; // Translation vector
    uint8_t sym;  // Symmetry to apply
};

}

std::size_t PieceState::alignment()
{
    return lcm(alignof(Piece), alignof(Transformation));
}

std::size_t PieceState::size(const ProcessedSpec &ps)
{
    std::size_t total_copies = 0;
    for (auto &sp : ps.pieces) total_copies += sp.colours.size();

    std::size_t size = 0;

    size += sizeof(Piece) * (ps.pieces.size() + 1);
    pad_to(size, alignof(Transformation));
    size += sizeof(Transformation) * total_copies;

    return size;
}

void PieceState::init(const ProcessedSpec &ps, uintptr_t m)
{
    // Get pointers to various things
    auto data = reinterpret_cast<Piece*>(m);
    auto pieces = data + 1;
    m += sizeof(Piece) * (ps.pieces.size() + 1);
    pad_to(m, alignof(Transformation));

    auto transformations = reinterpret_cast<Transformation*>(m);

    // Initialise memory
    new (data) Piece();

    auto p = pieces;
    auto t = transformations;

    for (auto &sp : ps.pieces)
    {
        new (p) Piece();
        p->trans = t;

        for (std::size_t i = 0; i < sp.colours.size(); i++)
        {
            new (t) Transformation();
            t++;
        }

        p++;
    }
}

void PieceState::update_counts(const std::vector<uint8_t> &counts, uintptr_t m)
{
    auto data = reinterpret_cast<Piece*>(m);
    auto pieces = data + 1;

    for (std::size_t i = 0; i < counts.size(); i++)
        pieces[i].count = counts[i];
}

void PieceState::reset_state(std::size_t num_pieces, uintptr_t m)
{
    auto data = reinterpret_cast<Piece*>(m);
    auto pieces = data + 1;

    // Reset used counts to zero, and rebuild linked list
    uint16_t cur = 0;

    for (std::size_t i = 0; i < num_pieces; i++)
    {
        pieces[i].used = 0;
        if (pieces[i].count > 0)
        {
            data[cur].next = 1 + i;
            data[1 + i].prev = cur;
            cur = 1 + i;
        }
    }

    data[cur].next = 0;
    data[0].prev = cur;
}

void PieceState::copy_state(std::size_t num_pieces, uintptr_t from, uintptr_t m)
{
    auto data = reinterpret_cast<Piece*>(m);
    auto pieces = data + 1;

    auto fdata = reinterpret_cast<Piece*>(from);
    auto fpieces = fdata + 1;

    data[0].prev = fdata[0].prev;
    data[0].next = fdata[0].next;

    for (std::size_t i = 0; i < num_pieces; i++)
    {
        pieces[i].used = fpieces[i].used;
        assert(pieces[i].count == fpieces[i].count);
        pieces[i].prev = fpieces[i].prev;
        pieces[i].next = fpieces[i].next;

        for (std::size_t j = 0; j < pieces[i].used; j++)
            pieces[i].trans[j] = fpieces[i].trans[j];
    }
}

void PieceState::place(uint8_t i, uint8_t x, uint8_t y, uint8_t sym, uintptr_t m)
{
    auto data = reinterpret_cast<Piece*>(m);
    auto &p = data[1 + i];

    // Store the transformation
    assert(p.used < p.count);
    p.trans[p.used] = {x, y, sym};

    // Update 'used' and remove from linked list if necessary
    p.used++;
    if (p.used == p.count)
    {
        data[p.prev].next = p.next;
        data[p.next].prev = p.prev;
    }
}

void PieceState::unplace(uint8_t i, uintptr_t m)
{
    auto data = reinterpret_cast<Piece*>(m);
    auto &p = data[1 + i];

    // Reinsert into the linked list at the point it was removed, if necessary
    if (p.used == p.count)
    {
        data[p.prev].next = 1 + i;
        data[p.next].prev = 1 + i;
    }

    assert(p.used > 0);
    p.used--;
}

uint16_t PieceState::begin_handle(uintptr_t m)
{
    auto data = reinterpret_cast<Piece*>(m);

    return data[0].next;
}

uint16_t PieceState::next_handle(uint16_t i, uintptr_t m)
{
    auto data = reinterpret_cast<Piece*>(m);

    return data[i].next;
}

void PieceState::readout_solution(std::size_t num_pieces, SolutionRecorder &recorder, uintptr_t m)
{
    auto data = reinterpret_cast<Piece*>(m);
    auto pieces = data + 1;

    auto orig_items_size = recorder.items.size();

    for (uint8_t i = 0; i < num_pieces; i++)
    {
        auto &p = pieces[i];
        assert(p.used == p.count);
        for (std::size_t j = 0; j < p.count; j++)
        {
            auto &t = p.trans[j];
            recorder.items.push_back({i, t.x, t.y, (Symmetry)t.sym});
        }
    }

    recorder.ranges.emplace_back(orig_items_size, recorder.items.size());
}
