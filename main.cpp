#include "spec.h"
#include "processedspec.h"
#include "solver.h"
#include "render.h"
#include "terminal.h"
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>

int main(int argc, char *argv[])
{
    // Get file paths
    if (argc != 2) {std::cerr << "Usage: " << argv[0] << " FILE\n"; return 1;}
    std::string spec_path = argv[1];
    std::string sol_path = spec_path + ".solution";
    std::string out_path = spec_path + ".pdf";

    // Load puzzle specification
    Spec spec;
    
    {
        std::ifstream in(spec_path);
        if (!in) {std::cerr << "Couldn't open that file\n"; return 1;}
        spec.read(in);
        if (!in) {std::cerr << "That file is malformed\n"; return 1;}
    }
    
    ProcessedSpec ps(spec);
    
    // Obtain the solution
    SolutionSet solutionset;
    
    auto try_load_cached_solution = [&]
    {
        std::ifstream in(sol_path);
        if (!in) return false;
        
        SolutionSet s;
        s.read(in);
        if (!in) {std::cerr << "Ignoring cached solution - malformed\n"; return false;}
        if (!s.validate(ps)) {std::cerr << "Ignoring cached solution - doesn't match puzzle\n"; return false;}
        
        solutionset = std::move(s);
        return true;
    };
    
    auto run_solver = [&]
    {
        std::string line;
        bool cancelled = false;
        
        solutionset = solve(ps, [&] (int count, double progress)
        {
            cancelled |= std::tolower(try_get_char()) == 'q';
            if (cancelled) return true;
            
            line = std::to_string(count) + (count == 1 ? " solution" : " solutions");
            line.resize(terminal_width(), ' ');
            
            int left = 20, right = line.length() - 1;
            if (right - left + 1 >= 5)
            {
                line[left ++] = '[';
                line[right--] = ']';
                int steps = right - left;
                steps = std::min<int>(steps, (steps + 1) * progress);
                for (int i = left; i <= left + steps; i++) line[i] = '=';
            }
            
            std::cout << '\r' << line;
            std::cout.flush();
            
            return false;
        });
        
        std::cout << '\n';
        return !cancelled;
    };
    
    auto try_save_cached_solution = [&]
    {
        std::ofstream out(sol_path);
        if (!out) {std::cerr << "Failed to save cached solution\n"; return;}
        solutionset.write(out);
    };
    
    if (try_load_cached_solution())
    {
        std::cout << "Using cached solution\n";
    }
    else if (run_solver())
    {
        try_save_cached_solution();
        std::cout << "Saved cached solution\n";
    }
    else
    {
        std::cerr << "Cancelled (some solutions might be missing)\n";
    }
    
    // Write solutions to output file
    SpecPolygons polygons(ps, 0.06);
    render_solutions(out_path.c_str(), solutionset, ps, polygons);

    return 0;
}
