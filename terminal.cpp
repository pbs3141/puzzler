#include "terminal.h"
#include <sys/ioctl.h>
#include <termios.h>
#include <unistd.h>

char try_get_char()
{
    termios term;
    tcgetattr(STDIN_FILENO, &term);
    
    termios term2 = term;
    term2.c_lflag &= ~ICANON;
    tcsetattr(STDIN_FILENO, TCSANOW, &term2);
    
    int byteswaiting;
    ioctl(STDIN_FILENO, FIONREAD, &byteswaiting);
    
    char ch = '\0';
    if (byteswaiting > 0) read(STDIN_FILENO, &ch, 1);
    
    tcsetattr(STDIN_FILENO, TCSANOW, &term);
    
    return ch;
}

int terminal_width()
{
    winsize sz;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &sz);
    return sz.ws_col;
}
