#pragma once

#include "processedspec.h"
#include "solution.h"
#include <functional>

SolutionSet solve(const ProcessedSpec &ps, std::function<bool(int, double)> callback = [] (int, double) {return false;});
