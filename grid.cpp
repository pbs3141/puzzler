#include "grid.h"
#include <istream>
#include <ostream>

void Grid::invert()
{
    squares.flip();
}

void Grid::trim()
{
    uint8_t x0 = 0;
    while (true)
    {
        if (x0 >= w) {w = h = 0; squares.clear(); return;}
        if (!iscolempty(x0)) break;
        x0++;
    }
    
    uint8_t y0 = 0;
    while (true)
    {
        if (y0 >= h) {w = h = 0; squares.clear(); return;}
        if (!isrowempty(y0)) break;
        y0++;
    }
    
    uint8_t w0 = w;
    while (iscolempty(w0 - 1)) w0--;
    
    uint8_t h0 = h;
    while (isrowempty(h0 - 1)) h0--;

    // Check if nothing to do
    if (x0 == 0 && y0 == 0 && w0 == w && h0 == h) return;

    // Perform trimming
    uint8_t neww = w0 - x0, newh = h0 - y0;
    std::vector<bool> newsquares(neww * newh);

    GridView newview(neww, newh);

    for (uint8_t x = 0; x < neww; x++)
        for (uint8_t y = 0; y < newh; y++)
            newsquares[newview.index(x, y)] = squares[index(x0 + x, y0 + y)];

    w = neww;
    h = newh;
    squares = std::move(newsquares);
}

bool Grid::isrowempty(uint8_t y) const
{
    for (uint8_t x = 0; x < w; x++)
        if (squares[index(x, y)]) return false;
    return true;
}

bool Grid::iscolempty(uint8_t x) const
{
    for (uint8_t y = 0; y < h; y++)
        if (squares[index(x, y)]) return false;
    return true;
}

std::vector<std::pair<Coord, uint8_t>> Grid::adjacents() const
{
    std::vector<std::pair<Coord, uint8_t>> result;

    for (int8_t x = -1; x <= w; x++)
        for (int8_t y = -1; y <= h; y++)
        {
            if (x >= 0 && y >= 0 && x < w && y < h && squares[index(x, y)]) continue;

            uint8_t a = 0;
            if (x >= 1 && y >= 0              && y < h     && squares[index(x - 1, y)]) a++;
            if (          y >= 0 && x < w - 1 && y < h     && squares[index(x + 1, y)]) a++;
            if (x >= 0 && y >= 1 && x < w                  && squares[index(x, y - 1)]) a++;
            if (x >= 0           && x < w     && y < h - 1 && squares[index(x, y + 1)]) a++;

            if (a > 0) result.emplace_back(Coord(x, y), a);
        }

    return result;
}

SymmetryGroup Grid::symmetry_subgroup(SymmetryGroup g) const
{
    uint8_t bitset = symmetry_bit(Symmetry::id);

    for (auto s : symmetrygroup_elements_nontrivial(g))
        if (compare_gridviews(*this, view(), *this, view().act(s)))
            bitset |= symmetry_bit(s);

    return symmetrygroup_from_bitset(bitset);
}

uint16_t Grid::count() const
{
    uint16_t result = 0;

    for (auto s : squares)
        if (s) result++;

    return result;
}

void Grid::read(std::istream &in)
{
    int w0, h0; in >> w0 >> h0;
    
    if (w0 < 0 || w0 > 0xff || h0 < 0 || h0 > 0xff) {in.setstate(std::ios::failbit); return;}
    
    w = w0;
    h = h0;
    squares.resize(w * h);
    
    auto v = view().act(Symmetry::refy);
    for (uint8_t y = 0; y < v.h; y++)
        for (uint8_t x = 0; x < v.w; x++)
        {
            char ch; in >> ch;
            squares[v.index(x, y)] = ch == 'X' || ch == 'x';
        }
}

void Grid::write(std::ostream &out) const
{
    out << (int)w << ' ' << (int)h;
    
    auto v = view().act(Symmetry::refy);
    for (uint8_t y = 0; y < v.h; y++)
    {
        out << '\n';
        for (uint8_t x = 0; x < v.w; x++)
            out << (squares[v.index(x, y)] ? 'X' : '.');
    }
}

bool compare_gridviews(const Grid &g1, GridView v1, const Grid &g2, GridView v2)
{
    if (v1.w != v2.w || v1.h != v2.h) return false;

    for (uint8_t x = 0; x < v1.w; x++)
        for (uint8_t y = 0; y < v1.h; y++)
            if (g1.squares[v1.index(x, y)] != g2.squares[v2.index(x, y)]) return false;

    return true;
}
