# puzzler

![classic](puzzles/classic-default-solution.png)

Puzzler is a solver for block puzzles. Although it can solve any puzzle, it was designed specifically to solve the one above, which is shown in its default configuration immediately after it was bought from a puzzle shop many years ago. It has [804 solutions](puzzles/classic.txt.pdf), of which the default one comes in at number 103. In addition to the main puzzle, there are also two bonus puzzles which originally could be obtained by flipping the tray upside down or using the lid. They are the small square and the two-squares variants:

![small square](puzzles/small-default-solution.png) ![two squares](puzzles/two-squares-default-solution.png)

Now you only have to use a subset of the pieces, but otherwise the rules are same: the pieces must completely fill the tray, no gaps. They have [140 solutions](puzzles/small.txt.pdf) and [78 solutions](puzzles/two-squares.txt.pdf) respectively.

Puzzler can also deal with different symmetry types. For example in the original puzzle, you might be tempted to cheat by flipping some of the pieces upside down. This corresponds to allowing reflections as well as rotations. In that case, you would find there are 69415, 1733, and 562 solutions respectively.

## Description

Puzzler's main novelty is that it is very fast. For example, it finds all 804 solutions to the main puzzle in under 0.5s. It does this using a number of tricks, including

* Parallel search - With work-stealing to ensure that all threads remain fully utilised to the end.
* Optimised memory layout - All data structures are kept in a single contiguous block of memory to improve cache locality, and each thread's writeable data is kept separate from the others and on separate cachelines to avoid false sharing.
* Symmetry reduction - Rather than discarding duplicate symmetry-related solutions, they aren't generated in the first place.
* Precomputation - As much data as possible is precomputed. This includes the subsets of pieces that have area equal to the tray, the allowed transformations that can be applied to them in order to perform symmetry reduction, the actual transformed pieces, their lists of border squares, and the lists of pieces that can be placed in a given tight spot and the orientations this can be done in.
* DLX - No block puzzle solver would be complete without using Dancing Links data structures to store the search state.
* Progress reporting and cancellability - The percentange of the search that is completed is updated in real time, and the search can be aborted quickly by pressing 'q'. This is done by adjusting the depth of the transition into non-interruptible fastsearch to meet responsivity targets.

Overall, it could be said that puzzler is a case study in optimisation gone too far.

## Known bugs

There is one unimplemented corner case in the symmetry reduction algorithm that could lead to duplicate symmetry-related solutions being found for highly symmetric puzzles. Although this isn't too hard to fix, doing so is very tedious, is almost never needed, and spoils the codebase. So it has been left unimplemented.
