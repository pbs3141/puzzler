#pragma once

#include "symmetry.h"
#include <cstdint>

struct Coord
{
    int8_t x, y;
    
    Coord() = default;
    Coord(int8_t x, int8_t y) : x(x), y(y) {}
    
    Coord act(Symmetry s, uint8_t w, uint8_t h) const;
};
