#pragma once

#include "coord.h"
#include "symmetry.h"
#include <cstdint>

struct GridView
{
    uint8_t w, h;
    
private:
    int8_t dx, dy;
    int32_t off;
    
public:
    GridView() = default;
    GridView(uint8_t w, uint8_t h)                                    : w(w), h(h), dx(1 ), dy(w ), off(0  ) {}
    GridView(uint8_t w, uint8_t h, int16_t off, int8_t dx, int8_t dy) : w(w), h(h), dx(dx), dy(dy), off(off) {}
    
    uint16_t index(int8_t x, int8_t y) const {return off + dx * (uint16_t)x + dy * (uint16_t)y;}
    uint16_t index(Coord p)            const {return index(p.x, p.y);}
    
    GridView act(Symmetry s) const;
};
