#pragma once

#include <vector>

/* Input:  A list of N pairs (nᵢ, aᵢ), and a target A.
 * Output: All tuples kᵢ such that
 *            0 <= kᵢ <= nᵢ
 *            Σ kᵢ aᵢ = A
 */

std::vector<std::vector<int>> subsetsums(const std::vector<int> &n, const std::vector<int> &a, int A);
