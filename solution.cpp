#include "solution.h"
#include <istream>
#include <ostream>

void Solution::read(std::istream &in)
{
    int num_items; in >> num_items;
    if (num_items < 0 || num_items > 10000) {in.setstate(std::ios::failbit); return;}

    items.resize(num_items);
    for (auto &item : items)
    {
        unsigned i, x, y, sym; in >> i >> x >> y >> sym;
        if (x >= 256 || y >= 256 || sym >= 8) {in.setstate(std::ios::failbit); return;}
        item.i = (uint8_t)i;
        item.x = (uint8_t)x;
        item.y = (uint8_t)y;
        item.sym = (Symmetry)sym;
    }
}

void Solution::write(std::ostream &out) const
{
    out << items.size();

    for (auto &item : items)
        out << '\n' << (int)item.i << ' ' << (int)item.x << ' ' << (int)item.y << ' ' << (int)item.sym;
}

bool Solution::validate(const ProcessedSpec &ps) const
{
    std::vector<int> squares(ps.grid.squares.size());
    for (uint16_t k = 0; k < squares.size(); k++)
        squares[k] = ps.grid.squares[k] ? -2 : -1;

    for (auto &item : items)
    {
        // Ensure piece index is valid
        if (item.i >= ps.pieces.size()) return false;

        auto &piece = ps.pieces[item.i];
        auto view = piece.grid.view().act(item.sym);

        // Ensure transformed piece lies within bounding box
        if (item.x + view.w > ps.grid.w || item.y + view.h > ps.grid.h) return false;

        for (uint8_t y = 0; y < view.h; y++)
            for (uint8_t x = 0; x < view.w; x++)
                if (piece.grid.squares[view.index(x, y)])
                {
                    uint16_t index = item.x + x + ps.grid.w * (uint16_t)(item.y + y);

                    // Ensure pieces don't overlap with background grid or with each other
                    if (squares[index] != -1) return false;

                    squares[index] = item.i;
                }
    }

    // Ensure no uncovered squares
    for (auto s : squares)
        if (s == -1)
            return false;

    return true;
}

// ------------------------------------------------------------------------- //

void SolutionSet::read(std::istream &in)
{
    int num_solutions; in >> num_solutions;
    if (num_solutions < 0 || num_solutions > 1000000) {in.setstate(std::ios::failbit); return;}

    solutions.resize(num_solutions);
    for (auto &sol : solutions)
        sol.read(in);
}

void SolutionSet::write(std::ostream &out) const
{
    out << solutions.size() << '\n';

    for (auto &sol : solutions)
    {
        out << '\n';
        sol.write(out);
        out << '\n';
    }
}

bool SolutionSet::validate(const ProcessedSpec &ps) const
{
    for (auto &sol : solutions)
        if (!sol.validate(ps)) return false;

    return true;
}

range<decltype(SolutionRecorder::items)::const_iterator> SolutionRecorder::get_solution(int i) const
{
    return make_range(items.begin() + ranges[i].first, items.begin() + ranges[i].second);
}
