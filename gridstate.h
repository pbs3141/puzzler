#pragma once

#include "grid.h"
#include <cstdint>

/*
 * A structure for keeping track of which squares in a grid are filled,
 * and for allowing fast retrieval of the most-surrounded empty square.
 *
 * To put a piece in the grid,
 *  *) all squares in the piece must be empty squares within the grid bounds
 *  1) call fill() for each square of the piece
 *  2) call adj() for each square adjacent to the piece
 *
 * To remove a piece from the grid,
 *  *) it must be the last piece added not yet removed
 *  1) call unadj() for each square adjacent to the piece
 *  2) call unfill() for each square of the piece
 */

struct GridState
{
    static std::size_t alignment();
    static std::size_t size(const Grid &grid);
    static void        init(const Grid &grid, uintptr_t m);

    static void reset_state(const Grid &grid, uintptr_t m);
    static void copy_state(uint16_t num_cells, uintptr_t from, uintptr_t m);

    static void   fill(uint16_t i,             uintptr_t m);
    static void unfill(uint16_t i,             uintptr_t m);
    static void    adj(uint16_t i, uint8_t da, uintptr_t m);
    static void  unadj(uint16_t i, uint8_t da, uintptr_t m);

    static bool filled(uint16_t i, uintptr_t m);
    static uint8_t adj(uint16_t i, uintptr_t m);

    static int16_t most_surrounded(uintptr_t m);
};
