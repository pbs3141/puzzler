#pragma once

#include "processedspec.h"
#include "range.h"

struct SolutionItem
{
    uint8_t i;     // Piece type, indexes into ProcessedSpec::pieces
    uint8_t x, y;  // Translation vector
    Symmetry sym;  // Symmetry to apply
};

struct Solution
{
    std::vector<SolutionItem> items;

    void read (std::istream&);
    void write(std::ostream&) const;

    bool validate(const ProcessedSpec&) const;
};

struct SolutionRecorder
{
    std::vector<std::pair<int, int>> ranges;
    std::vector<SolutionItem> items;

    std::size_t num_solutions() const {return ranges.size();}
    range<decltype(items)::const_iterator> get_solution(int i) const;
};

struct SolutionSet
{
    std::vector<Solution> solutions;

    void read (std::istream&);
    void write(std::ostream&) const;

    bool validate(const ProcessedSpec&) const;
};
