#include "symmetry.h"
#include <stdexcept>

Symmetry symmetry_composition(Symmetry s1, Symmetry s2)
{
    auto o1 = (int)s1 & 0b11, t1 = (int)s1 >> 2;
    auto o2 = (int)s2 & 0b11, t2 = (int)s2 >> 2;

    auto o = o1 + (t1 ? 4 - o2 : o2); o %= 4;
    auto t = t1 + t2;                 t %= 2;

    auto s = o | t << 2;
    return (Symmetry)s;
}

Symmetry symmetry_inverse(Symmetry s)
{
    auto t = (int)s >> 2;
    if (t == 1) return s;

    auto o = (int)s;
    return (Symmetry)((4 - o) % 4);
}

// ----------------

constexpr const char* symmetry_name_data[] =
{
    "id",
    "rot90",
    "rot180",
    "rot270",
    "refx",
    "refmdiag",
    "refy",
    "refpdiag"
};

const char* symmetry_name(Symmetry s)
{
    return symmetry_name_data[(int)s];
}

// ----------------

constexpr const char* symmetrygroup_name_data[] =
{
    "trivial",
    "rot180",
    "rot90",
    "refx",
    "refy",
    "refpdiag",
    "refmdiag",
    "refxy",
    "refdiag",
    "full"
};

const char* symmetrygroup_name(SymmetryGroup g)
{
    return symmetrygroup_name_data[(int)g];
}

// ----------------

constexpr Symmetry symmetrygroup_element_data[] =
{
    #define s Symmetry

    s::id,
    s::id, s::rot180,
    s::id, s::rot90, s::rot180, s::rot270,
    s::id, s::refx,
    s::id, s::refy,
    s::id, s::refpdiag,
    s::id, s::refmdiag,
    s::id, s::refx, s::refy, s::rot180,
    s::id, s::refpdiag, s::refmdiag, s::rot180,
    s::id, s::rot90, s::rot180, s::rot270, s::refx, s::refy, s::refpdiag, s::refmdiag

    #undef s
};

constexpr std::size_t symmetrygroup_range_data[] =
{
    0, 1, 3, 7, 9, 11, 13, 15, 19, 23, 31
};

range<const Symmetry*> symmetrygroup_elements(SymmetryGroup g)
{
    return {symmetrygroup_element_data + symmetrygroup_range_data[(int)g    ],
            symmetrygroup_element_data + symmetrygroup_range_data[(int)g + 1]};
}

range<const Symmetry*> symmetrygroup_elements_nontrivial(SymmetryGroup g)
{
    return {symmetrygroup_element_data + symmetrygroup_range_data[(int)g    ] + 1,
            symmetrygroup_element_data + symmetrygroup_range_data[(int)g + 1]    };
}

// ----------------

uint8_t symmetry_bit(Symmetry s)
{
    return (uint8_t)1 << (int)s;
}

SymmetryGroup symmetrygroup_from_bitset(uint8_t bitset)
{
    switch (bitset)
    {
    case 0b00000001: return SymmetryGroup::trivial;
    case 0b00000101: return SymmetryGroup::rot180;
    case 0b00001111: return SymmetryGroup::rot90;
    case 0b00010001: return SymmetryGroup::refx;
    case 0b01000001: return SymmetryGroup::refy;
    case 0b10000001: return SymmetryGroup::refpdiag;
    case 0b00100001: return SymmetryGroup::refmdiag;
    case 0b01010101: return SymmetryGroup::refxy;
    case 0b10100101: return SymmetryGroup::refdiag;
    case 0b11111111: return SymmetryGroup::full;
    default:         throw std::logic_error("symmetrygroup_from_bitset: bitset does not correspond to any subgroup");
    }
}

// ----------------

constexpr uint8_t symmetrygroup_bitset_data[] =
{
    0b00000001,
    0b00000101,
    0b00001111,
    0b00010001,
    0b01000001,
    0b10000001,
    0b00100001,
    0b01010101,
    0b10100101,
    0b11111111
};

uint8_t symmetrygroup_to_bitset(SymmetryGroup g)
{
    return symmetrygroup_bitset_data[(int)g];
}

SymmetryGroup symmetrygroup_conjugate(SymmetryGroup g, Symmetry h)
{
    auto hinv = symmetry_inverse(h);

    uint8_t bitset = symmetry_bit(Symmetry::id);
    for (auto s : symmetrygroup_elements_nontrivial(g))
    {
        auto s2 = symmetry_composition(h, symmetry_composition(s, hinv));
        bitset |= symmetry_bit(s2);
    }

    return symmetrygroup_from_bitset(bitset);
}

SymmetryGroup symmetrygroup_intersect(SymmetryGroup g1, SymmetryGroup g2)
{
    return symmetrygroup_from_bitset(symmetrygroup_to_bitset(g1) & symmetrygroup_to_bitset(g2));
}
