cmake_minimum_required(VERSION 3.7)
project(puzzler CXX)

set(CMAKE_CXX_STANDARD 14)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra -Wpedantic")

find_package(Threads REQUIRED)

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}")
find_package(Cairo REQUIRED)
include_directories(${CAIRO_INCLUDE_DIR})

add_executable(puzzler coord.cpp grid.cpp gridstate.cpp gridview.cpp main.cpp pieceinfo.cpp piecestate.cpp polygons.cpp processedspec.cpp render.cpp solution.cpp solver.cpp spec.cpp subsetsums.cpp symmetry.cpp terminal.cpp)
target_link_libraries(puzzler Threads::Threads ${CAIRO_LIBRARIES})
