#include "subsetsums.h"
#include <stdexcept>

struct SubsetSumSearch
{
    SubsetSumSearch(const std::vector<int> &n, const std::vector<int> &a, int target) : n(n), a(a), k(n.size())
    {
        if (n.size() != a.size())
            throw std::logic_error("subsetsums: sizes of n and a are unequal");

        int avail = 0;
        for (std::size_t i = 0; i < n.size(); i++)
            avail += n[i] * a[i];

        if (avail < target)
        {
            // Unfeasible
            return;
        }
        else if (avail == target)
        {
            // Only one solution using every piece
            ks = {n};
            return;
        }

        search(target, avail, 0);
    }

    std::vector<std::vector<int>>& result() {return ks;}

private:
    const std::vector<int> &n, &a;

    std::vector<int> k;
    std::vector<std::vector<int>> ks;

    void search(int target, int avail, std::size_t i)
    {
        if (target > avail || target < 0) return;

        if (i == n.size())
        {
            if (target == 0) ks.emplace_back(k);
            return;
        }

        for (k[i] = n[i]; k[i] >= 0; k[i]--)
        {
            search(target - k[i] * a[i], avail - n[i] * a[i], i + 1);
        }
    }
};

std::vector<std::vector<int>> subsetsums(const std::vector<int> &n, const std::vector<int> &a, int target)
{
    return SubsetSumSearch(n, a, target).result();
}
