#pragma once

#include "processedspec.h"
#include "solution.h"
#include <cstdint>
#include <vector>

/*
 * A structure for keeping track of how many copies of each piece have been placed,
 * and the transformations that have been applied to them,
 * and for allowing iteration over the unused pieces.
 *
 * Calls to place/unplace must be always be last-in first-out.
 *
 * Must call 'update_counts' to set the effective piece counts to use. This can be done
 * multiple times, but invalidates any previous state, so must be followed by a call to
 * 'reset_state' or 'copy_state'. In order to copy state, the piece counts must be the same.
 *
 * The optimisation focus is on insertion/removal/iteration, while extracting the
 * finished solution once found is less important.
 */

struct PieceState
{
    static std::size_t alignment();
    static std::size_t size(const ProcessedSpec &ps);
    static void        init(const ProcessedSpec &ps, uintptr_t m);

    static void update_counts(const std::vector<uint8_t> &counts, uintptr_t m);
    static void reset_state(std::size_t num_pieces, uintptr_t m);
    static void copy_state (std::size_t num_pieces, uintptr_t from, uintptr_t m);

    static void   place(uint8_t i, uint8_t x, uint8_t y, uint8_t sym, uintptr_t m);
    static void unplace(uint8_t i,                                    uintptr_t m);

    static uint16_t begin_handle (uintptr_t m);
    static bool     is_end_handle(uint16_t i) {return i == 0;}
    static uint16_t next_handle  (uint16_t i, uintptr_t m);
    static uint16_t index_handle (uint16_t i) {return i - 1;}

    static void readout_solution(std::size_t num_pieces, SolutionRecorder &recorder, uintptr_t m);
};
