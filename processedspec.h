#pragma once

#include "spec.h"

struct ProcessedSpec
{
    // (background grid)
    Grid grid;

    // Number of empty squares
    uint16_t area;
    
    // (symmetries that can be applied to pieces)
    SymmetryGroup symmetrygroup;

    // Subgroup of 'symmetrygroup' that preserves the background grid
    SymmetryGroup grid_subgroup;
    
    struct Piece
    {
        // (piece squares)
        Grid grid;

        // Subgroup of 'symmetrygroup' preserving the piece
        SymmetryGroup subgroup;

        // Adjacent squares forming a 1-square-thick border around the piece, and the adjacency count of these squares
        std::vector<std::pair<Coord, uint8_t>> adj;

        // (number of copies and their colours)
        std::vector<uint32_t> colours;

        // Number of squares
        uint16_t area;

        // All distinct orientations of the piece obtained by acting on it with 'symmetrygroup'
        std::vector<Symmetry> transformations;

        // The complete list of Fits - a Fit describes a way of transforming and translating the piece so it fits into a square with given surroundings (only those with adjacency >= 2 are counted) ('trans' indexes into Piece::transformations)
        struct Fit {uint8_t surr; uint8_t tr; uint8_t x, y;};
        std::vector<Fit> fits;
    };
    
    std::vector<Piece> pieces;

    struct Subset
    {
        // The piece counts
        std::vector<uint8_t> counts;

        // The total number of pieces, including multiplicities
        uint16_t numpieces;

        // The restricted set of piece orientations
        std::vector<std::vector<Symmetry>> transformations;

        // Residual symmetries remaining even after orientation-fixing
        SymmetryGroup residual;
    };

    // Subsets of pieces whose combined area equals that of the grid
    std::vector<Subset> subsets;

    // The maximum of 'numpieces' over all subsets
    uint16_t max_numpieces;
    
    ProcessedSpec(const Spec&);
};
