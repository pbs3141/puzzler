#include "render.h"
#include <cairo/cairo.h>
#include <cairo/cairo-pdf.h>

namespace {

void cairo_move_to(cairo_t *cr, std::pair<double, double> p) {cairo_move_to(cr, p.first, p.second);}
void cairo_line_to(cairo_t *cr, std::pair<double, double> p) {cairo_line_to(cr, p.first, p.second);}

cairo_matrix_t symmetry_to_cairo_transform(Symmetry s, double w, double h)
{
    switch (s)
    {
        case Symmetry::id:       return { 1.0,  0.0,  0.0,  1.0, 0.0, 0.0};
        case Symmetry::rot90:    return { 0.0,  1.0, -1.0,  0.0, h,   0.0};
        case Symmetry::rot180:   return {-1.0,  0.0,  0.0, -1.0, w,   h  };
        case Symmetry::rot270:   return { 0.0, -1.0,  1.0,  0.0, 0.0, w  };
        case Symmetry::refx:     return {-1.0,  0.0,  0.0,  1.0, w,   0.0};
        case Symmetry::refy:     return { 1.0,  0.0,  0.0, -1.0, 0.0, h  };
        case Symmetry::refpdiag: return { 0.0,  1.0,  1.0,  0.0, 0.0, 0.0};
        case Symmetry::refmdiag: return { 0.0, -1.0, -1.0,  0.0, h,   w  };
    }
}

void cairo_symmetry(cairo_t *cr, Symmetry s, uint8_t w, uint8_t h)
{
    auto mat = symmetry_to_cairo_transform(s, w, h);
    cairo_transform(cr, &mat);
}

void cairo_polygon(cairo_t *cr, const PiecePolygons::Polygon &pts)
{
    cairo_move_to(cr, pts[0]);
    for (std::size_t i = 1; i < pts.size(); i++)
        cairo_line_to(cr, pts[i]);
    cairo_close_path(cr);
}

void cairo_polygons(cairo_t *cr, const PiecePolygons &polys)
{
    cairo_new_path(cr);
    for (auto &p : polys.polygons)
        cairo_polygon(cr, p);
}

void draw_solution(cairo_t *cr, const Solution &sol, const ProcessedSpec &ps, const SpecPolygons &polygons, std::vector<int> &counters)
{
    // Reset counters for each type of piece
    for (auto &x : counters)
        x = 0;

    // Draw black outlines
    cairo_set_source_rgb(cr, 0.0, 0.0, 0.0);
    cairo_polygons(cr, polygons.grid);
    cairo_fill(cr);

    // Draw the coloured interior of each piece
    for (auto &item : sol.items)
    {
        auto &piece = ps.pieces[item.i];
        auto col = piece.colours[counters[item.i]++];
        uint8_t r =  col        & 0xff;
        uint8_t g = (col >>  8) & 0xff;
        uint8_t b = (col >> 16) & 0xff;

        cairo_set_source_rgb(cr, r / 255.0, g / 255.0, b / 255.0);
        cairo_save(cr);
        cairo_translate(cr, item.x, item.y);
        cairo_symmetry(cr, item.sym, piece.grid.w, piece.grid.h);
        cairo_polygons(cr, polygons.pieces[item.i]);
        cairo_restore(cr);
        cairo_fill(cr);
    }
}

}

void render_solutions(const char *filename, const SolutionSet &solutionset, const ProcessedSpec &ps, const SpecPolygons &polygons)
{
    constexpr double imgscale = 15;

    double w = ps.grid.w + 2 * polygons.edge;
    double h = ps.grid.h + 2 * polygons.edge;

    auto surface = cairo_pdf_surface_create(filename, w * imgscale, h * imgscale);
    auto cr = cairo_create(surface);

    // Transform solution grid to [0..w] × [0..h]
    cairo_scale(cr, imgscale, imgscale);
    cairo_translate(cr, 0.0, h);
    cairo_scale(cr, 1.0, -1.0);

    // Transform solution grid minus border to [0..ps.grid.w, 0..ps.grid.h]
    cairo_translate(cr, polygons.edge, polygons.edge);

    std::vector<int> counters(ps.pieces.size()); /* keep out of loop */
    for (auto &sol : solutionset.solutions)
    {
        draw_solution(cr, sol, ps, polygons, counters);
        cairo_show_page(cr);
    }
    
    cairo_destroy(cr);
    cairo_surface_destroy(surface);
}
