#pragma once

#include "coord.h"
#include "symmetry.h"
#include "grid.h"
#include <vector>
#include <iosfwd>

struct Spec
{
    // The grid into which pieces can be placed, along with the already-filled squares
    Grid grid;
    
    // The symmetries we may apply to our pieces
    SymmetryGroup symmetrygroup;
    
    // The puzzle pieces
    struct Piece
    {
        // The squares occupied by the piece
        Grid grid;
        
        // How many copies, and their colours
        std::vector<uint32_t> colours;
    };
    
    std::vector<Piece> pieces;
    
    void read (std::istream&);
    void write(std::ostream&) const;
};
