#include "coord.h"

Coord Coord::act(Symmetry s, uint8_t w, uint8_t h) const
{
    switch (s)
    {
        case Symmetry::id:       return *this;
        case Symmetry::rot90:    return Coord(h - 1 - y,         x);
        case Symmetry::rot180:   return Coord(w - 1 - x, h - 1 - y);
        case Symmetry::rot270:   return Coord(        y, w - 1 - x);
        case Symmetry::refx:     return Coord(w - 1 - x,         y);
        case Symmetry::refy:     return Coord(        x, h - 1 - y);
        case Symmetry::refpdiag: return Coord(        y,         x);
        case Symmetry::refmdiag: return Coord(h - 1 - y, w - 1 - x);
    }
}
